
   class Teste{

	public static void main(String [] args){
	
	   Funcionario f1 = new Funcionario();

	   f1.nome = "Filipe";
	   f1.departamento = "A";
	   f1.salario = 1000;
	   f1.dataEntrada = "01/01/2014";
	   f1.rg = "2776";
	   
	   f1.recebeAumento(50);
	   f1.mostra();

	   Funcionario f2 = new Funcionario();

           f2.nome = "Carlos";
           f2.departamento = "B";
           f2.salario = 1500;
           f2.dataEntrada = "02/02/2014";
           f2.rg = "2556";

           f2.recebeAumento(50);
           f2.mostra();

	   Funcionario f3 = new Funcionario();

           f3.nome = "Filipe";
           f3.departamento = "A";
           f3.salario = 1000;
           f3.dataEntrada = "01/01/2014";
           f3.rg = "2776";

           f3.recebeAumento(50);
           f3.mostra();

/* Quando é criado um novo objeto (new) a comparação do IF não é válida, pois todo novo objeto
ID, então será sempre 'diferentes'*/

	if(f1 == f3){
		System.out.println("iguais");
	} else{
		System.out.println("diferentes");
	}

   }


}
